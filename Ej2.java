package practica3;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import java.awt.event.ActionListener;

public class Ej2 {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	int numero;
	String resultado;
	private final Action action = new SwingAction();
	private JButton button;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ej2 window = new Ej2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ej2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	
	
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 288);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblInsertarNumero = new JLabel("Insertar Numero:");
		lblInsertarNumero.setFont(new Font("Cambria", Font.PLAIN, 14));
		lblInsertarNumero.setBounds(39, 49, 120, 25);
		panel.add(lblInsertarNumero);
		
		textField = new JTextField();
		textField.setFont(new Font("Cambria Math", Font.PLAIN, 11));
		textField.setBounds(39, 74, 86, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		JButton btnProcesar = new JButton("Procesar");
		btnProcesar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				numero = Integer.parseInt(textField.getText());
				if (numero % 2 == 0){
					resultado = "El numero ingresado es par";
				}
				else{
					resultado = "El numero ingresado es impar";
				}
			textField_1.setText(String.valueOf(resultado));
			
			}
			
		});
		btnProcesar.setAction(action);
		btnProcesar.setBounds(244, 62, 101, 33);
		panel.add(btnProcesar);
		
		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setBackground(SystemColor.menu);
		textField_1.setBounds(93, 126, 252, 33);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		button = new JButton("Aceptar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textField_1.setText(null);
				textField.setText(null);
			}
		});
		button.setBounds(165, 192, 101, 33);
		panel.add(button);
	}

	@SuppressWarnings("serial")
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "Procesar");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
}
