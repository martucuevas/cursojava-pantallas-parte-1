package practica3;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import java.awt.Font;

public class Ej3UtilizandoIF {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private final Action action = new SwingAction();

	/**
	 * Launch the application.
	 */
	String Mes;
	private JButton btnNewButton_1;
	private final Action action_1 = new SwingAction_1();
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ej3UtilizandoIF window = new Ej3UtilizandoIF();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ej3UtilizandoIF() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 583, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		textField = new JTextField();
		textField.setFont(new Font("Cambria", Font.PLAIN, 12));
		textField.setBounds(50, 75, 120, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblMesDelAo = new JLabel("Ingrese el mes (en minusculas): ");
		lblMesDelAo.setBounds(50, 50, 189, 14);
		panel.add(lblMesDelAo);
		
		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setBounds(46, 137, 401, 28);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.setAction(action);
		btnNewButton.setBounds(265, 75, 89, 23);
		panel.add(btnNewButton);
		
		btnNewButton_1 = new JButton("New button");
		btnNewButton_1.setAction(action_1);
		btnNewButton_1.setBounds(183, 200, 89, 23);
		panel.add(btnNewButton_1);
	}

	@SuppressWarnings("serial")
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "Ejecutar");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		Mes = textField.getText();
		if (Mes.equals("abril") || Mes.equals("junio") || Mes.equals("septiembre") || Mes.equals("noviembre")){
				textField_1.setText(String.valueOf("El mes de "+Mes+" tiene 30 Dias"));	
		}else{ 
			if(Mes.equals ("febrero")){
				textField_1.setText(String.valueOf("El mes de "+Mes+" tiene 28 Dias o 29 Dias en a�os biciestos"));
				}else{ 
					if(Mes.equals("enero") || Mes.equals("marzo") || Mes.equals("mayo") || Mes.equals("julio") || Mes.equals("agosto") || Mes.equals("octubre") || Mes.equals("diciembre")){
						textField_1.setText(String.valueOf("El mes de "+Mes+" tiene 31 Dias"));
				}else{
					    textField_1.setText(String.valueOf("El mes es invalido"));
		
		
		}
		
		
	}
}
		}
	}
	@SuppressWarnings("serial")
	private class SwingAction_1 extends AbstractAction {
		public SwingAction_1() {
			putValue(NAME, "Aceptar");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
			textField_1.setText(null);
			textField.setText(null);
		}
	}
}
			
					
			
				
