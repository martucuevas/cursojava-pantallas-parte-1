package practica3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;


public class Ej1Pantalla {
			
	private JFrame frmNotasAlumno;
	private JTextField txtNota1;
	private JTextField txtNota2;
	private JTextField txtNota3;
	 

	/**
	 * Launch the application.
	 */
	
    Double promedio, Nota1, Nota2, Nota3; 
    String CONDICION; 
    private JTextField textField;
    private JTextField textField_1;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ej1Pantalla window = new Ej1Pantalla();
					window.frmNotasAlumno.setVisible(true); 	
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

	/**
	 * Create the application.
	 */
	public Ej1Pantalla() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmNotasAlumno = new JFrame();
		frmNotasAlumno.setTitle("Notas Alumno");
		frmNotasAlumno.setBounds(100, 100, 530, 384);
		frmNotasAlumno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmNotasAlumno.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		frmNotasAlumno.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new GridLayout(1, 1, 3, 3));
		
		JLabel lblBienvenidoProfesor = new JLabel(".: BIENVENIDO PROFESOR :.");
		lblBienvenidoProfesor.setForeground(SystemColor.desktop);
		lblBienvenidoProfesor.setBackground(SystemColor.textHighlight);
		lblBienvenidoProfesor.setHorizontalAlignment(SwingConstants.CENTER);
		lblBienvenidoProfesor.setFont(new Font("Arial", Font.PLAIN, 15));
		panel.add(lblBienvenidoProfesor);
		
		JPanel panel_1 = new JPanel();
		frmNotasAlumno.getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(null);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBounds(0, 37, 434, 171);
		panel_1.add(panel_4);
		panel_4.setLayout(null);
		
		JLabel lblNota = new JLabel("Nota 1:");
		lblNota.setBounds(30, 23, 46, 14);
		panel_4.add(lblNota);
					
		txtNota1 = new JTextField();
		txtNota1.setBounds(30, 38, 86, 20);
		panel_4.add(txtNota1);
		txtNota1.setColumns(10);
		
		
		txtNota2 = new JTextField();
		txtNota2.setBounds(30, 83, 86, 20);
		panel_4.add(txtNota2);
		txtNota2.setColumns(10);
		
		
		JLabel lblNota_1 = new JLabel("Nota 2:");
		lblNota_1.setBounds(30, 69, 46, 14);
		panel_4.add(lblNota_1);
		
		
		txtNota3 = new JTextField();
		txtNota3.setLocation(30, 128);
		panel_4.add(txtNota3);
		txtNota3.setSize(new Dimension(86, 20));
		txtNota3.setColumns(10);
		
		
		JLabel label = new JLabel("Nota 3:\r\n");
		label.setBounds(30, 114, 46, 14);
		panel_4.add(label);
		
		JButton btnCalcular = new JButton("CALCULAR");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Nota1 = Double.parseDouble(txtNota1.getText());
				Nota2 = Double.parseDouble(txtNota2.getText());
				Nota3 = Double.parseDouble(txtNota3.getText());
				
				promedio = Nota1 + Nota2 + Nota3;
				promedio = promedio / 3; 
			    promedio = Math.round(promedio * 100)/ 100d;
				
				if(promedio < 7){
					CONDICION = "DESAPROBADO";
					textField_1.setForeground(Color.RED);
				}
				else{
					CONDICION = "APROBADO";
					textField_1.setForeground(Color.GREEN);
				}
				
				textField.setText(String.valueOf(promedio));
				textField_1.setText(String.valueOf(CONDICION));
				
				
			}
		});
		btnCalcular.setFont(new Font("Cambria Math", Font.PLAIN, 12));
		btnCalcular.setBounds(239, 82, 89, 23);
		panel_4.add(btnCalcular);
		
		
		
		JLabel lblNewLabel = new JLabel("Ingrese las tres notas corresondientes:\r\n\r\n");
		lblNewLabel.setBounds(0, 0, 434, 34);
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 12));
		lblNewLabel.setForeground(new Color(0, 0, 0));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panel_1.add(lblNewLabel);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(0, 15, -3, 193);
		panel_1.add(panel_3);
		panel_3.setLayout(new GridLayout(3, 1, 3, 30));
		
		JLabel lblPromedio = new JLabel("PROMEDIO: ");
		lblPromedio.setBounds(0, 223, 75, 27);
		panel_1.add(lblPromedio);
		lblPromedio.setFont(new Font("Cambria Math", Font.PLAIN, 14));
		
		
		JLabel lblCondicion = new JLabel("CONDICION: ");
		lblCondicion.setBounds(0, 261, 95, 34);
		panel_1.add(lblCondicion);
		lblCondicion.setFont(new Font("Cambria Math", Font.PLAIN, 14));
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBackground(SystemColor.menu);
		textField.setBounds(93, 219, 138, 27);
		panel_1.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setFont(new Font("Arial Black", Font.PLAIN, 14));
		textField_1.setBackground(SystemColor.menu);
		textField_1.setBounds(93, 261, 138, 27);
		panel_1.add(textField_1);
		textField_1.setColumns(10);
	
	   
	}
	}

